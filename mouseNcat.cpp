///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Nathaniel Murray <murrayn@hawaii.edu>
/// @date    1/27/2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {

   //initialize i
   int i = argc-1;

   //prints arguments to argument 0
   if(argc>1){
      
      //iterate over argc in reverse, terminates at i = -1

      for(int i = argc-1; i>=0; i--){

         if(i == 0){
            std::cout << "Argument " << i << " = " << "[" << argv[i]+2 << "]" << std::endl;
         }

         else{
         
         //i-1 --> count of arguments starts at 0
    	   std::cout << "Argument " << i << " = " << "[" << argv[i] << "]" << std::endl;
         }
      }  
   }

   //if no arguments, print environment variables
   else{

      //iterate over envp until envp is NULL 
      for (char **env = envp; *env != 0; env++){
      
      char *thisEnv = *env;
      printf("%s\n", thisEnv);    
      }
   }

std::exit( EXIT_SUCCESS );

}
